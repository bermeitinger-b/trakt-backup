import logging
from dataclasses import dataclass, field
from pathlib import Path

from omegaconf import DictConfig, OmegaConf

logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s", level=logging.INFO)

_log = logging.getLogger("trakt.config")

__CONFIG_FILE = Path.cwd().joinpath("config.yaml")


@dataclass
class TraktConfig:
    api_url: str = "https://api.trakt.tv"
    client_id: str | None = None
    client_secret: str | None = None
    access_token: str | None = None
    refresh_token: str | None = None
    last_refresh: str | None = None


@dataclass
class DiffConfig:
    timezone: str = "Europe/Berlin"


@dataclass
class EmailConfig:
    sender: str | None = None
    receiver: str | None = None
    host: str | None = None
    port: int | None = None
    user: str | None = None
    password: str | None = None


@dataclass
class Config:
    trakt: TraktConfig = field(default_factory=TraktConfig)
    diff: DiffConfig = field(default_factory=DiffConfig)
    email: EmailConfig = field(default_factory=EmailConfig)


def write_config(config: DictConfig) -> None:
    with __CONFIG_FILE.open("w") as fo:
        OmegaConf.save(config=config, f=fo)


def read_config() -> DictConfig:
    default = OmegaConf.structured(Config)
    if not __CONFIG_FILE.exists():
        _log.warning("Config file does not exist, will create the default one.")
        config = default
    else:
        try:
            _log.info(f"Loading ConfigFile from: {__CONFIG_FILE}")
            with __CONFIG_FILE.open("r") as fp:
                config = OmegaConf.load(fp)

            config = OmegaConf.merge(default, config)
        except Exception as e:
            _log.error("Error during reading config file", e)
            raise ConfigError() from e
    write_config(config)
    return config


class ConfigError(Exception):
    def __init__(self, *msgs: str):
        self.message = " - ".join(msg for msg in msgs if len(msg) > 0)
