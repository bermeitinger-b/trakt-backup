import argparse
import datetime as dt
import logging
import sys
from pathlib import Path

import pandas as pd
from jinja2 import Template
from omegaconf import DictConfig

from . import __version__
from .api import TraktEntry
from .config import ConfigError, read_config
from .template import get_template

__DESCRIPTION = """A tool to compare two trakt backups and emails the difference."""

_log = logging.getLogger("trakt.diff")


def _parser() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description=__DESCRIPTION)

    parser.add_argument(
        "-f",
        "--folder",
        help="Folder in which to look for files to diff",
        type=str,
        default="backups",
    )

    parser.add_argument(
        "-m",
        "--message",
        help="Type of message to print",
        type=str,
        choices=["cli", "email"],
        default="cli",
    )

    return parser.parse_args()


def _read_config() -> DictConfig:
    try:
        config = read_config()
    except ConfigError as e:
        logging.error("Can't read the config file: %s", e)
        sys.exit(1)
    return config


def read_csv(csv_file: str | Path) -> pd.DataFrame:
    df = pd.read_csv(csv_file)

    df["entry_type"] = df["entry_type"].astype("category")
    df["watched_at"] = df["watched_at"].astype(pd.Int64Dtype())
    df["movie_title"] = df["movie_title"].astype("category")
    df["show_title"] = df["show_title"].astype("category")
    df["season_number"] = df["season_number"].astype(pd.Int64Dtype())
    df["episode_number"] = df["episode_number"].astype(pd.Int64Dtype())
    df["episode_title"] = df["episode_title"].astype("string")

    return df


def compare(old: pd.DataFrame, new: pd.DataFrame) -> tuple[pd.DataFrame, pd.DataFrame]:
    merged = old.merge(new, how="outer", indicator=True)
    added_entries = merged[merged["_merge"] == "right_only"].copy()
    removed_entries = merged[merged["_merge"] == "left_only"].copy()

    added_entries = added_entries.drop(columns=["_merge"])
    added_entries = added_entries.sort_values(by="watched_at", ascending=False)
    removed_entries = removed_entries.drop(columns=["_merge"])
    removed_entries = removed_entries.sort_values(by="watched_at", ascending=False)

    return added_entries, removed_entries


_LEN_DATE = len(dt.date.today().isoformat())
_LEN_DATETIME = len(dt.datetime.now().isoformat())


def _parse_date(name: str) -> dt.datetime:
    try:
        return dt.datetime.fromisoformat(name[:_LEN_DATETIME])
    except ValueError:
        return dt.datetime.fromisoformat(name[:_LEN_DATE])


def main(options: argparse.Namespace, config: DictConfig) -> None:
    folder = Path(options.folder)
    if not folder.exists() or not folder.is_dir():
        msg = f"This folder is not a folder: {folder}"
        raise ValueError(msg)

    # reading
    csvs = [(_parse_date(csv.name), csv) for csv in folder.glob("*.csv.*")]

    if (csv_amount := len(csvs)) >= 2:  # noqa: PLR2004
        _log.info(f"Found {csv_amount} csv files.")
    else:
        msg = "Can't build a diff with less than 2 csv file."
        _log.error(msg)
        raise ValueError(msg)

    csvs = sorted(csvs, key=lambda csv: csv[0], reverse=True)

    (new_date, new_csv), (old_date, old_csv) = csvs[0], csvs[1]
    _log.info(f"Will compare the backups from {old_date.isoformat()} to newest {new_date.isoformat()}")

    new_df, old_df = read_csv(new_csv), read_csv(old_csv)

    added_entries, removed_entries = compare(old_df, new_df)

    message = generate_message(options, config, added_entries, removed_entries, old_date, new_date)

    send_message(options, config, message)


def generate_message(
    options: argparse.Namespace,
    config: DictConfig,
    added_entries: pd.DataFrame,
    removed_entries: pd.DataFrame,
    old_date: dt.datetime,
    new_date: dt.datetime,
) -> str:
    added_movies = [TraktEntry(*tpl[1:]) for tpl in added_entries[added_entries.entry_type == "movie"].itertuples()]
    removed_movies = [
        TraktEntry(*tpl[1:]) for tpl in removed_entries[removed_entries.entry_type == "movie"].itertuples()
    ]
    added_episodes = [TraktEntry(*tpl[1:]) for tpl in added_entries[added_entries.entry_type == "episode"].itertuples()]
    removed_episodes = [
        TraktEntry(*tpl[1:]) for tpl in removed_entries[removed_entries.entry_type == "episode"].itertuples()
    ]

    template = Template(source=get_template(options.message))

    message = template.render(
        diff_size=sum(map(len, (added_entries, removed_entries))),
        old_date=old_date.strftime("%A, %Y-%m-%d %H:%M:%S"),
        new_date=new_date.strftime("%A, %Y-%m-%d %H:%M:%S"),
        added_movies=added_movies,
        removed_movies=removed_movies,
        added_episodes=added_episodes,
        removed_episodes=removed_episodes,
        tz=config.get("diff").get("timezone"),
        version=__version__,
    )

    return message


def send_message(options: argparse.Namespace, config: DictConfig, message: str) -> None:
    match options.message:
        case "cli":
            print(message)  # noqa: T201
        case "email":
            __send_email(config["email"], message)
        case _:
            msg = f"message type '{options.message}' unknown"
            raise ValueError(msg)


def __send_email(config: DictConfig, message: str) -> None:
    from envelope import Envelope

    diff_size = message.split()[0]

    msg = Envelope()

    msg.smtp(
        {
            "host": config.get("host"),
            "port": config.get("port"),
            "user": config.get("user"),
            "password": config.get("password"),
        }
    )

    msg.from_(config.get("sender"))
    msg.to(config.get("receiver"))
    msg.subject(f"{diff_size} New Trakt Difference{'' if diff_size == 1 else 's'}")

    msg.message(message)

    response = msg.send()

    if not response:
        _log.error("There was an error sending the email.")


if __name__ == "__main__":
    options = _parser()
    config = _read_config()
    main(options, config)
