import argparse
import asyncio
import datetime as dt
import logging
import sys
from pathlib import Path

import pandas as pd

from .api import Trakt, TraktEntry, TraktError
from .config import ConfigError, read_config

__DESCRIPTION = """This program export your whole history (movies/episodes) from Trakt.tv"""

__EPILOG = """Export Trakt.tv history (series or movies) to CSV file."""

logging.basicConfig(format="%(asctime)s [%(levelname)s] %(message)s", level=logging.INFO)

_log = logging.getLogger("trakt.backup")


async def main() -> None:
    # Parse inputs if any
    parser = argparse.ArgumentParser(description=__DESCRIPTION, epilog=__EPILOG)

    parser.add_argument(
        "-f",
        "--folder",
        help="Folder in which to save the output csv file (defaults to %(default)s)",
        type=str,
        default="backups",
    )

    options = parser.parse_args()

    # Read configuration and validate
    try:
        config = read_config()
    except ConfigError as e:
        _log.error(f"Error during reading the config file: {e.message}")
        sys.exit(1)

    try:
        trakt = Trakt(config)
        export_data: list[TraktEntry] = await trakt.get_history()
    except TraktError as e:
        _log.error(f"Can't connect to Trakt: {e.message}")
        sys.exit(1)

    today = dt.datetime.today().isoformat()

    if len(export_data) > 0:
        _log.info(f"Found {len(export_data)} items")
    else:
        _log.error("Nothing found.")
        sys.exit(1)

    df = pd.DataFrame(export_data)

    df["season_number"] = df["season_number"].astype(pd.Int64Dtype())
    df["episode_number"] = df["episode_number"].astype(pd.Int64Dtype())

    output = Path(options.folder).joinpath(f"{today}_history.csv.zst")

    _log.info(f"Writing output file to {output}")
    df.to_csv(output, index=False)


if __name__ == "__main__":
    asyncio.run(main())
