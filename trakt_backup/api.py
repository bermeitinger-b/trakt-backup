import asyncio
import datetime as dt
import logging
import os
from dataclasses import dataclass
from typing import Any

import httpx
import pandas as pd
from babel.dates import format_date, format_time
from omegaconf import DictConfig

from .config import write_config

_log = logging.getLogger("trakt.api")
_locale = os.environ.get("LANG", None)
if _locale is None:
    _locale = "en_US.UTF-8"
_locale = _locale.split(".")[0]

# Trakt api is sometimes slow
DEFAULT_TIMEOUT = httpx.Timeout(timeout=30.0, connect=60.0)
# Limit number of elements to 500 per page
PAGINATION_LIMIT = 500
# Request only 8 pages concurrently
CONCURRENT_LIMIT = asyncio.Semaphore(8)


@dataclass
class TraktEntry:
    trakt_id: int
    entry_type: str
    watched_at: int
    movie_title: str | None
    show_title: str | None
    season_number: str | None
    episode_number: str | None
    episode_title: str | None

    def watched_at_fmt(self, tz: str) -> str:
        watched = pd.to_datetime(self.watched_at, unit="s").tz_localize("UTC").tz_convert(tz)

        return f"{format_date(watched.date(), locale=_locale)} {format_time(watched.time(), locale=_locale)}"


class TraktError(Exception):
    def __init__(self, *msgs: str):
        self.message = " - ".join(msg for msg in msgs if len(msg) > 0)


class Trakt:
    _default_refresh_rate = dt.timedelta(hours=24)  # token is valid for 24 hours

    _default_headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "User-Agent": "Trakt-Backup//0.1",
        "Connection": "Keep-Alive",
        "trakt-api-version": "2",
    }

    _default_client = "urn:ietf:wg:oauth:2.0:oob"
    _default_auth_url = (
        "https://trakt.tv/oauth/authorize?response_type=code&redirect_uri=urn:ietf:wg:oauth:2.0:oob&client_id={}"
    )
    _default_api_url = "https://api.trakt.tv"

    _default_auth_headers = {"Content-Type": "application/json"}

    _default_auth_data = {
        "client_id": None,
        "client_secret": None,
        "redirect_uri": _default_client,
        "grant_type": None,
    }

    def __init__(self, global_config: DictConfig):
        self._global_config = global_config
        self._config = global_config["trakt"]

        try:
            self.authenticate()
        except TraktError as e:
            _log.error("Can't authenticate")
            raise e

        self._headers = Trakt._default_headers.copy()
        self._headers.update(
            {
                "trakt-api-key": self._config["client_id"],
                "Authorization": f"Bearer {self._config['access_token']}",
            }
        )

    def authenticate(self) -> None:
        if not all((self._config.get("client_id"), self._config.get("client_secret"))):
            msg = "You have to set the client_id and client_secret."
            raise RuntimeError(msg)
        if access_token := self._config.get("access_token"):
            # need refresh?
            if (
                dt.datetime.now() - dt.datetime.fromisoformat(self._config.get("last_refresh"))
                >= Trakt._default_refresh_rate
            ):
                _log.info("API needs new access_token using the refresh token.")
                access_token, refresh_token, created_at = self._refresh()
            else:
                return
        else:
            _log.info("No access_token found, will try to authenticate.")
            access_token, refresh_token, created_at = self._authenticate()

        self._config["access_token"] = access_token
        self._config["refresh_token"] = refresh_token
        self._config["last_refresh"] = created_at.isoformat()
        write_config(self._global_config)

    def _authenticate(self) -> tuple[str, str, dt.datetime]:
        if (client_id := self._config.get("client_id")) == "":
            msg = "client_id is not set"
            raise ValueError(msg)
        if (client_secret := self._config.get("client_secret")) == "":
            msg = "client_secret is not set"
            raise ValueError(msg)

        auth_url = Trakt._default_auth_url.format(client_id)

        print(f"Go to this url:\n {auth_url}")  # noqa: T201

        pin = input("PIN: ")

        data = Trakt._default_auth_data.copy()
        data.update(
            {
                "client_id": client_id,
                "client_secret": client_secret,
                "grant_type": "authorization_code",
                "code": pin,
            }
        )

        url = f"{Trakt._default_api_url}/oauth/token"
        _log.info(f"Will send oauth request to {url}")
        _log.info(data)
        r = httpx.post(url, json=data, timeout=DEFAULT_TIMEOUT)
        if r.status_code != httpx.codes.OK:
            msg = f"Can't get OAuth-Token: {r.status_code}"
            raise TraktError(msg, f"{r.text}")
        response = r.json()
        access_token, refresh_token = (
            response["access_token"],
            response["refresh_token"],
        )

        return access_token, refresh_token, dt.datetime.now()

    def _refresh(self) -> tuple[str, str, dt.datetime]:
        url = f"{Trakt._default_api_url}/oauth/token"

        if (client_id := self._config.get("client_id")) == "":
            msg = "client_id is not set"
            raise ValueError(msg)
        if (client_secret := self._config.get("client_secret")) == "":
            msg = "client_secret is not set"
            raise ValueError(msg)
        if (refresh_token := self._config.get("refresh_token")) == "":
            msg = "refresh_token is not set"
            raise ValueError(msg)

        data = Trakt._default_auth_data.copy()
        data.update(
            {
                "client_id": client_id,
                "client_secret": client_secret,
                "refresh_token": refresh_token,
                "grant_type": "refresh_token",
            }
        )

        r = httpx.post(url, json=data, timeout=DEFAULT_TIMEOUT)
        if r.status_code != httpx.codes.OK:
            msg = f"Can't refresh access_token: {r.status_code}"
            raise TraktError(msg, f"{r.text}")
        response = r.json()
        access_token, refresh_token, created_at = (
            response["access_token"],
            response["refresh_token"],
            dt.datetime.fromtimestamp(response["created_at"]),
        )

        return access_token, refresh_token, created_at

    async def get_history(self) -> list[TraktEntry]:
        url = f"{Trakt._default_api_url}/sync/history"

        content: list[TraktEntry] = []

        # Reset client
        client = httpx.AsyncClient(headers=self._headers, timeout=DEFAULT_TIMEOUT)

        tasks = []
        try:
            # get first page
            first_page, total_page_count = await self._get_page(client=client, url=url, page=1)

            content.extend(first_page)

            if total_page_count <= 1:
                return content

            async with asyncio.TaskGroup() as tg:
                for i in range(2, total_page_count + 1):
                    tasks.append(tg.create_task(self._get_page(client=client, url=url, page=i)))
        finally:
            await client.aclose()

        for task in tasks:
            content.extend(task.result()[0])

        return content

    async def _get_page(
        self, client: httpx.AsyncClient, url: str, page: int, tries: int = 5
    ) -> tuple[list[TraktEntry], int]:
        _log.info(f"Requesting page {page:>3}")

        params = {"page": page, "limit": PAGINATION_LIMIT}

        await CONCURRENT_LIMIT.acquire()
        r = await client.get(url, params=params)
        CONCURRENT_LIMIT.release()
        if r.status_code != httpx.codes.OK:
            _log.error(f"Can't fetch {page=}: {r.status_code}")
            if tries <= 0:
                msg = "Can't fetch page, tried too often."
                raise TraktError(msg)
            return await self._get_page(client=client, url=url, page=page, tries=tries - 1)

        response = r.json()

        entries = self._parse_entries(response)

        return entries, int(r.headers.get("X-Pagination-Page-Count", 1))

    @staticmethod
    def _parse_date(date_string: str) -> int:
        return int(dt.datetime.fromisoformat(date_string).timestamp())

    def _parse_entries(self, page_data: list[dict[str, Any]]) -> list[TraktEntry]:
        entries = []

        for data in page_data:
            entry_type = data["type"]
            is_movie = entry_type == "movie"
            t = TraktEntry(
                trakt_id=data[entry_type]["ids"]["trakt"],
                entry_type=entry_type,
                watched_at=self._parse_date(data["watched_at"]),
                movie_title=data["movie"]["title"] if is_movie else None,
                show_title=data["show"]["title"] if not is_movie else None,
                season_number=data["episode"]["season"] if not is_movie else None,
                episode_number=data["episode"]["number"] if not is_movie else None,
                episode_title=data["episode"]["title"] if not is_movie else None,
            )

            entries.append(t)

        return entries
