# syntax=docker/dockerfile:1.4
FROM python:3.13-slim as base

SHELL ["/bin/bash", "-e", "-u", "-o", "pipefail", "-c"]

ENV USER_NAME="trakt"
ENV USER_ID="1000"
ENV USER_HOME="/home/${USER_NAME}"
ENV APP_ROOT="/app"
ENV PIP_CACHE="${USER_HOME}/.cache/pip"

ENV PYTHONUNBUFFERED="1"
ENV PYTHONDONTWRITEBYTECODE="1"
ENV PYTHONIOENCODING="UTF-8"

RUN \
  --mount=type=cache,sharing=locked,target=/var/cache/apt \
  --mount=type=cache,sharing=locked,target=/var/lib/apt \
  <<EOF

  # Setup
  export DEBIAN_FRONTEND="noninteractive"
  echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections
  rm -f /etc/apt/apt.conf.d/docker-clean
  echo 'Binary::apt::APT::Keep-Downloaded-Packages "true";' > /etc/apt/apt.conf.d/keep-cache

  # Install basic dependencies
  apt-get update -q
  apt-get install --yes --no-install-recommends \
      "gosu" \
      "locales" \
      "libmagic1"

  # Use english layout for everything
  echo "en_US.UTF-8 UTF-8" > /etc/locale.gen
  locale-gen

  useradd \
      --create-home \
      --no-log-init \
      --shell "/bin/bash" \
      --uid "${USER_ID}" \
      "${USER_NAME}"

  mkdir -p "${APP_ROOT}"
  chown -R "${USER_NAME}:${USER_NAME}" "${APP_ROOT}"

  mkdir -p "${PIP_CACHE}"
  chown -R "${USER_NAME}:${USER_NAME}" "${PIP_CACHE}"

  # Done.
EOF
ENV LANG="en_US.UTF-8"

FROM base as uv
RUN \
  --mount=type=cache,sharing=locked,target=/var/cache/apt \
  --mount=type=cache,sharing=locked,target=/var/lib/apt \
  <<EOF
  # Install basic dependencies
  apt-get update -q
  apt-get install --yes --no-install-recommends \
      "git"

  # Done.
EOF

ENV UV_CACHE_DIR="${USER_HOME}/.cache/uv"
ENV UV_COMPILE_BYTECODE=1
ENV UV_LINK_MODE=copy

# Install uv via their docker image
COPY --from=ghcr.io/astral-sh/uv:latest /uv /uvx /usr/local/bin/

WORKDIR "${APP_ROOT}"
USER "${USER_NAME}"
COPY --chown="${USER_NAME}" . "${APP_ROOT}"
RUN \
  --mount=type=cache,uid="${USER_ID}",target="${UV_CACHE_DIR}" \
  <<EOF
  
  uv build

  # Done.
EOF

FROM base as final

COPY --from=uv --link --chown="${USER_}" "${APP_ROOT}/dist" "/tmp"
WORKDIR "${APP_ROOT}"
USER "${USER_NAME}"
RUN \
  --mount=type=cache,uid="${USER_ID}",target="${PIP_CACHE}" \
  --mount=type=bind,from=uv,source="${APP_ROOT}/dist",target="/tmp/wheels" \
  <<EOF

  # Install
  find /tmp/wheels -iname "*.whl" | \
    xargs pip install --user
  
  # Done.
EOF

COPY entrypoint.sh /usr/local/bin/entrypoint.sh
ENTRYPOINT ["bash", "/usr/local/bin/entrypoint.sh"]
USER root
