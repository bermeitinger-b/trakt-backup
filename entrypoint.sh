#!/usr/bin/env bash
set -euo pipefail
echo "Welcome to TraktBackup"

# Run bash by default
if [[ $# -eq 0 ]]; then
    inner=( "bash" )
else
    inner=( "$@" )
fi

cmd=( "exec" )

if [[ "${USER_ID}" != "0" ]]; then

    groupmod \
        --gid "${USER_ID}" \
        "${USER_NAME}"

    usermod \
        --uid "${USER_ID}" \
        --gid "${USER_ID}" \
        "${USER_NAME}"

    # We are running as user
    cmd+=( "gosu" "${USER_NAME}" )

else
    echo "You're running this as root"
    export PYTHONPATH="${USER_HOME}/.local/lib/python3.13/site-packages"
fi

# Run command
"${cmd[@]}" "${inner[@]}"
