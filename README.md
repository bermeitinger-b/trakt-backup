# Trakt.tv Export Tool

## Purpose
Export Trakt history and compare.

## Requirements
You must use Python >= 3.10 (Development is done with 3.11).

## Usage
* Create an [Trakt.tv application](https://trakt.tv/oauth/applications) to have your own ``client_id`` and ``client_secret``, https://trakt.tv/oauth/applications.
You only need to fill up the ``Name`` with a ``Description`` and ``Redirect uri`` to `urn:ietf:wg:oauth:2.0:oob`, leave the rest empty and click on ``SAVE APP``.

* Run the script to create a default config file ``config.yaml``

```sh
python -m trakt_backup.backup 
```

* Edit the config file ``config.yaml`` and specify the ``client_id`` and ``client_secret`` as well as any other settings appropriate to your enviromenent.
Refer to ``Configuration details`` section for more information.

### Export history:
```sh
python -m trakt_backup.backup
```

### Compare version:
This compares the most recent backup with the one before.

```sh
python -m trakt_backup.diff -m cli
```

If you set it up to send email:
```sh
python -m trakt_backup.diff -m email
```

## Docker:
The `docker-compose.yaml` shows the mounted volumes and that there are two services: backup and diff.
Backup does the actual backup and diff prints (by default to cli) the difference between the last 2 versions.
You should use a `docker-compose.override.yaml`:
```text
services:
    diff:
        environment:
            USER_ID: "1000"  # Use your UID (id -u) 
            LANG: "de_DE.UTF8"
        command: ["python", "-m", "trakt_backup.diff", "-m", "email"]
```

It will allow you to set the `LANG` environment variable which will be used for the date formatting and in the `command`
part, that you want to receive emails.

## systemd timer:
Run it every day at 23:59:59:
```text
# /etc/systemd/system/trakt.timer
[Unit]
Description=Run Trakt Backup and Diff

[Timer]
OnCalendar=*-*-* 23:59:59
Persistent=true

[Install]
WantedBy=multi-user.target
```

And the service file: (assuming, you set it in the docker-compose.yml file correctly)
```text
# /etc/systemd/system/trakt.service
[Unit]
Description=Trakt Backup And Diff Notification Service
Wants=trakt.timer

[Service]
WorkingDirectory=/docker/trakt
ExecStart=/bin/bash -c "/usr/local/bin/docker-compose run --rm backup && /usr/local/bin/docker-compose run --rm diff"

[Install]
WantedBy=trakt.timer
```

## License
This script is free software:  you can redistribute it and/or  modify  it under  the  terms  of the  GNU  General  Public License  as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the  implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

See <http://www.gnu.org/licenses/gpl.html>.

## Acknowledgement
This project started from a fork of [xpgmsharp/trakt](https://github.com/xbgmsharp/trakt) but it is different now.
